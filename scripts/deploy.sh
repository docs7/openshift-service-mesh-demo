#!/bin/sh
oc apply -f artifacts/frontend-v1-deployment.yml -n project-1
oc apply -f artifacts/backend-v1-deployment.yml -n project-2
oc apply -f artifacts/backend-v2-deployment.yml -n project-2
oc apply -f artifacts/backend-service.yml -n project-2
oc apply -f artifacts/frontend-service.yml -n project-1
oc apply -f artifacts/frontend-route.yml -n project-1
export FRONTEND_URL=http://$(oc get route frontend -n project-1 -o jsonpath='{.status.ingress[0].host}')
echo "Frontend URL:${FRONTEND_URL}"
