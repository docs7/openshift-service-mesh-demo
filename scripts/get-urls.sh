#!bin/sh
export FRONTEND_URL=$(oc get route frontend -n project-1 -o jsonpath='{.spec.host}')
export GATEWAY_URL=$(oc get route istio-ingressgateway -o jsonpath='{.spec.host}' -n demo-istio-system)
export KIALI_URL=$(oc get route kiali -o jsonpath='{.spec.host}' -n demo-istio-system)
export JAEGER_URL=$(oc get route jaeger -o jsonpath='{.spec.host}' -n demo-istio-system)
echo "FRONTEND_URL=http://$(oc get route frontend -n project-1 -o jsonpath='{.spec.host}')"
echo "GATEWAY_URL=http://$(oc get route istio-ingressgateway -o jsonpath='{.spec.host}' -n demo-istio-system)"
echo "KIALI_URL=https://$(oc get route kiali -o jsonpath='{.spec.host}' -n demo-istio-system)"
echo "JAEGER_URL=https://$(oc get route jaeger -o jsonpath='{.spec.host}' -n demo-istio-system)"
