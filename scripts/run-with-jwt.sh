#!/bin/sh
TOKEN=$(cat artifacts/jwt.txt)
while [ 1  ];
do
  OUTPUT=$(curl -H "Authorization: Bearer ${TOKEN}" $GATEWAY_URL -s -w "Elapsed Time:%{time_total}")
  HOST=$(echo $OUTPUT|awk -F'Host:' '{print $2}'| awk -F',' '{print $1}')
  VERSION=$(echo $OUTPUT|awk -F'Backend version:' '{print $2}'| awk -F',' '{print $1}')
  RESPONSE=$(echo $OUTPUT|awk -F',' '{print $2}' | awk -F':' '{print $2}')
  TIME=$(echo $OUTPUT| awk -F"Elapsed Time:" '{print $2}'|awk -F'#' '{print $1}')
  echo "Backend:$VERSION, Response Code:$RESPONSE, Host:$HOST, Elapsed Time:$TIME sec"
done

