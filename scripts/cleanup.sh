#!/bin/sh
oc delete policy  --all -n project-1
oc delete policy  --all -n project-2
oc delete gateway --all  -n project-1
oc delete serviceentry --all  -n project-2
oc delete destinationrule --all  -n project-1
oc delete destinationrule --all  -n project-2
oc delete virtualservice --all  -n project-2
oc delete virtualservice --all  -n project-1
oc delete smmr --all -n demo-istio-system
oc delete smcp --all -n demo-istio-system