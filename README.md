# Red Hat OpenShift Service Mesh Demo

![banner](images/ossm-banner.png)

<!-- TOC -->

- [Red Hat OpenShift Service Mesh Demo](#red-hat-openshift-service-mesh-demo)
  - [Application Architecture](#application-architecture)
  - [Setup](#setup)
  - [Observability](#observability)
  - [Dynamic Routing](#dynamic-routing)
    - [Weighted Routing](#weighted-routing)
    - [Dark Launch (Mirror)](#dark-launch-mirror)
    - [Matching Routing](#matching-routing)
  - [Fault Tolerance](#fault-tolerance)
    - [Timeout](#timeout)
    - [Circuit Breaker with Pool Ejection](#circuit-breaker-with-pool-ejection)
  - [Service Security](#service-security)
    - [Secure with Mutual TLS (mTLS)](#secure-with-mutual-tls-mtls)
      - [Backend Application](#backend-application)
      - [Frontend Application](#frontend-application)
    - [Secure Frontend with JWT Authentication](#secure-frontend-with-jwt-authentication)
    - [Istio Egress Gateway](#istio-egress-gateway)
  - [Cleanup](#cleanup)

<!-- /TOC -->

## Application Architecture
```mermaid
graph LR
    client-->frontend
    frontend-->backend-v1
    frontend-->backend-v2
    subgraph project-1
    frontend
    end
    subgraph project-2
    backend-v1
    backend-v2
    end
    backend-v1-->https://httpbin.org/status/200
    backend-v2-->https://httpbin.org/delay/5
```

## Setup

![overall archiecture](images/overall-architecture.png)

- Create Project
```bash
oc new-project project-1 --description="Frontend App" --display-name="Project 1"
oc new-project project-2 --description="Backend App" --display-name="Project 2"
oc new-project demo-istio-system --description="Istio Control Plane" --display-name="Istio Control Plane"
```
- Deploy frontend app and backend app
```bash
scripts/deploy.sh
```
- Install Operators
  - ElasticSearch
  - Jaeger
  - Kiali
  - OpenShift Service Mesh
- Create control plane
```bash
oc apply -f artifacts/basic-install.yml -n demo-istio-system
oc project demo-istio-system
watch oc get pods
```
- Join project-1 and project-2 to control plane
```bash
oc apply -f artifacts/memberroll.yml -n demo-istio-system
```
- Set annotation for istio sidecar injection for frontend with Dev Console
- Set annotation for istio sidecar injection for backend with oc command
```bash
oc patch deployment backend-v1 -p '{"spec":{"template":{"metadata":{"annotations":{"sidecar.istio.io/inject":"true"}}}}}' -n project-2
oc patch deployment backend-v2 -p '{"spec":{"template":{"metadata":{"annotations":{"sidecar.istio.io/inject":"true"}}}}}' -n project-2
```

## Observability
- Overview - project/namespace level with overall information
  
  ![](images/kiali-overview.png)
  
- Kiali graph
  
  ![](images/kiali-graph.png)
  
- Applications

  ![](images/kiali-applications.png)

- Workload -> Inbound Metrics and Outbound Metrics

  ![](images/kiali-applications-metrics.png)
  
- Workload -> open Grafana
- Service -> Traffic/Inbound Metrics
- Service -> Traces 
- Istio Config
- Distributed Tracing

## Dynamic Routing 

### Weighted Routing

Use Kiali to set virtual service and destination rules for backend with 80/20 weight for v1 and v2 respectively

```mermaid
graph LR
    client-->frontend
    frontend--80%-->backend-v1
    frontend--20%-->backend-v2
    subgraph project-1
    frontend
    end
    subgraph project-2
    backend-v1
    backend-v2
    end
    backend-v1-->https://httpbin.org/status/200
    backend-v2-->https://httpbin.org/delay/6
```


- Service -> Backend -> Action -> Create Weighted Routing

![weighted routing](images/create-weighted-routing-menu.png)

- Set weight to 80/20 and save 

![set weighted](images/set-weighted-80-20.png)

- This can also configure by following YAML files
```bash
#!/bin/sh
oc apply -f artifacts/destination-rule-backend-v1-v2.yml -n project-2
oc apply -f artifacts/virtual-service-backend-v1-v2-80-20.yml -n project-2
```
- Run test script [run-20.sh](scripts/run-20.sh)
```bash
source scripts/get-urls.sh
scripts/run-20.sh
```

### Dark Launch (Mirror)
- Dark launch. deploy backend-v3 and test that request not send to backend-v3
```bash
oc apply -f artifacts/backend-v3-deployment.yml -n project-2
oc apply -f artifacts/backend-v3-service.yml -n project-2
```
- Use Kiali to add mirror to backend-v3 by edit backend's virtual service
```yaml
http:
    - mirror:
        host: backend-v3.project-2.svc.cluster.local
      route:
        - destination:
            host: backend.project-2.svc.cluster.local
            subset: v1
          weight: 80
        - destination:
            host: backend.project-2.svc.cluster.local
            subset: v2
          weight: 20
```
- Test with cURL and check backend-v3 log (from Developer Console or Kialia Console)
```bash
curl $FRONTEND_URL
curl $FRONTEND_URL
```
- Remove mirror from destination rule (with Kiali)
- Remove backend-v3 deployment and service
```bash
oc delete -f artifacts/backend-v3-deployment.yml -n project-2
oc delete -f artifacts/backend-v3-service.yml -n project-2
```

### Matching Routing
```mermaid
graph LR
    client-->istio-gateway
    istio-gateway--User-Agent = curl-->frontend-v2
    istio-gateway--User-Agent = Other-->frontend-v1
    subgraph demo-istio-system
    istio-gateway
    end

    frontend-v1-->backend-v1
    frontend-v2-->backend-v1
    subgraph project-1
    frontend-v1
    frontend-v2
    end
    subgraph project-2
    backend-v1
    end
    backend-v1-->https://httpbin.org/status/200
```
- Deploy frontend v2
```bash
oc apply -f artifacts/frontend-v2-deployment.yml -n project-1
```
- Create matching rules by Kiali
    -  Service -> Frontend -> Action -> Create Matching Routing

        ![](images/create-matching-routing.png)

    -  Add rule for User-Agent prefix with curl will route to frontend-v2

        ![](images/add-rule-for-user-agent-01.png)

        ![](images/add-rule-for-user-agent-02.png)

        ![](images/add-rule-for-user-agent-03.png)
        
    -  Add rule for others will route to frontend-v1

        ![](images/add-rule-for-user-agent-04.png)
  
    -  Configure gateway. VirtualService Host set to asterisk (*)

        ![](images/create-gateway.png)

- Create matching rules by YAML 
    - Create destination rule and virtual service for frontend
    ```bash
    oc apply -f artifacts/destination-rule-frontend-v1-v2.yml -n project-1
    oc apply -f artifacts/virtual-service-frontend-header-curl-to-v2.yml -n project-1
    ```
    - Create Gateway
    ```bash
    oc apply -f artifacts/frontend-gateway.yml -n project-1
    ```
- Test with cURL will get response from Frontend v2
```bash
curl $GATEWAY_URL
```
- Test with other client will get response from Frontend v1
```bash
curl -H "User-Agent: foo" $GATEWAY_URL
```

## Fault Tolerance 

### Timeout

```mermaid
graph LR
    client-->frontend
    frontend--timeout=3s-->backend-v1
    frontend--timeout=3s-->backend-v2
    subgraph project-1
    frontend
    end
    subgraph project-2
    backend-v1
    backend-v2
    end
    backend-v1-->https://httpbin.org/status/200
    backend-v2-->https://httpbin.org/delay/6
```

- Use Kiali to add timeout by edit backend's virtual service
```yaml
http:
    - route:
        - destination:
            host: backend.project-2.svc.cluster.local
            subset: v1
          weight: 50
        - destination:
            host: backend.project-2.svc.cluster.local
            subset: v2
          weight: 50
      timeout: 3s
```
- Run test script [run-20.sh](scripts/run-20.sh)

### Circuit Breaker with Pool Ejection

```mermaid
graph LR
    client-->frontend
    frontend--CB with Pool Ejection-->backend-v1
    subgraph project-1
    frontend
    end
    subgraph project-2
    backend-v1
    end
    backend-v1-->https://httpbin.org/status/200
```
**Remark:**

With outlier detection enabled. Envoy will automatically enable locality features. With locality enabled, envoy will connect to pod within same **topology.kubernetes.io/region** and **topology.kubernetes.io/zone** respectively. 

Then we for this demo, we need to place all backend-v1 pods within same zone by using nodeSelector

Check for zone in your environment by run following command
```bash
oc get node -l topology.kubernetes.io/zone=ap-southeast-1a,node-role.kubernetes.io/worker=
oc get node -l topology.kubernetes.io/zone=ap-southeast-1b,node-role.kubernetes.io/worker=
oc get node -l topology.kubernetes.io/zone=ap-southeast-1c,node-role.kubernetes.io/worker=
```


This can be done by in frontend deployment template is configured  with topology.kubernetes.io/zone ap-southeast-1a and backend is configured with topology.kubernetes.io/zone ap-southeast-1b
```yaml
spec:
  containers:
  ...
  nodeSelector:
    topology.kubernetes.io/zone: ap-southeast-1b
```

- Set weighted v1 to 100% (Use kiali to adjust weight) and scale backend-v1 to 3 pods (Dev Console)
- Use terminal to connect to one of three pod and run following command to force one pod to return 504
```bash
curl -w"\n" http://127.0.0.1:8080/stop
curl -w"\n" -v  http://127.0.0.1:8080/
```
![set liveness to false](images/set-liveness-to-false.png)

- Run test script [run-20.sh](scripts/run-20.sh)
```bash
Backend:v1, Response Code: 200, Host:backend-v1-65c6975f9d-jz8nd, Elapsed Time:1.133997 sec
Backend:v1, Response Code: 504, Host:backend-v1-65c6975f9d-dfm2d, Elapsed Time:0.108555 sec
Backend:v1, Response Code: 200, Host:backend-v1-65c6975f9d-ckx8w, Elapsed Time:1.060745 sec
Backend:v1, Response Code: 200, Host:backend-v1-65c6975f9d-jz8nd, Elapsed Time:0.348947 sec
Backend:v1, Response Code: 504, Host:backend-v1-65c6975f9d-dfm2d, Elapsed Time:0.115460 sec
Backend:v1, Response Code: 200, Host:backend-v1-65c6975f9d-ckx8w, Elapsed Time:0.344789 sec
Backend:v1, Response Code: 200, Host:backend-v1-65c6975f9d-jz8nd, Elapsed Time:0.343925 sec
Backend:v1, Response Code: 504, Host:backend-v1-65c6975f9d-dfm2d, Elapsed Time:0.102829 sec
```
- Set outlier detection with pool ejection to backend's destination rule
```bash
oc apply -f artifacts/destination-rule-backend-circuit-breaker-with-pool-ejection.yml -n project-2
```
- Detailed outlier detection
```yaml
spec:
  host: backend
  trafficPolicy:
    connectionPool:
      http: {}
      tcp: {}
    loadBalancer:
      simple: ROUND_ROBIN
    outlierDetection:
      baseEjectionTime: 5m
      consecutiveErrors: 1
      interval: 5m
      maxEjectionPercent: 100
```
- Run test script [run-20.sh](scripts/run-20.sh). This time we will receive only one 504 response because pool ejection policy.
```bash
Backend:v1, Response Code: 504, Host:backend-v1-7cc84fdf5b-p2cjt, Elapsed Time:0.219416 sec
Backend:v1, Response Code: 200, Host:backend-v1-7cc84fdf5b-6p4sp, Elapsed Time:0.832561 sec
Backend:v1, Response Code: 200, Host:backend-v1-7cc84fdf5b-lqbbl, Elapsed Time:1.088108 sec
Backend:v1, Response Code: 200, Host:backend-v1-7cc84fdf5b-6p4sp, Elapsed Time:0.342818 sec
Backend:v1, Response Code: 200, Host:backend-v1-7cc84fdf5b-lqbbl, Elapsed Time:0.341540 sec
Backend:v1, Response Code: 200, Host:backend-v1-7cc84fdf5b-6p4sp, Elapsed Time:0.340572 sec
```
- Set backend-v1 replicas to 1
```bash
oc scale deployment/backend-v1 --replicas=0 -n project-2
oc scale deployment/backend-v1 --replicas=1 -n project-2
```


## Service Security

### Secure with Mutual TLS (mTLS)
```mermaid
graph LR
    client-->istio-gateway
    istio-gateway--mTLS-->frontend-v2
    istio-gateway--mTLS-->frontend-v1
    subgraph demo-istio-system
    istio-gateway
    end

    frontend-v1--mTLS-->backend-v1
    frontend-v2--mTLS-->backend-v1
    subgraph project-1
    frontend-v1
    frontend-v2
    end
    subgraph project-2
    backend-v1
    end
    backend-v1-->https://httpbin.org/status/200
```

#### Backend Application

- Apply mTLS authentication policy to backend service
```bash
oc apply -f artifacts/destination-rule-backend-v1-v2-mtls.yml -n project-2
oc apply -f artifacts/authentication-backend-enable-mtls.yml -n project-2
```
- Deploy application without sidecar
```bash
oc apply -f artifacts/station-deployment.yml -n project-1
```
- Use Developer Console to run cURL from station pod's terminal 
```bash
curl -w"\n" -v http://frontend:8080/version
curl -w"\n" -v http://backend.project-2.svc.cluster.local:8080/version
```
- Use Developer Console to run cURL from frontend pod's terminal
```bash
curl -v http://backend.project-2.svc.cluster.local:8080/version
```
- Test cURL to $FRONTEND_URL and $GATEWAY_URL. both are sucessful

#### Frontend Application

- Access frontend app from route
```bash
curl $FRONTEND_URL
```
- Apply mTLS authentication policy to frontend service
```bash
oc apply -f artifacts/destination-rule-frontend-v1-v2-mtls.yml -n project-1
oc apply -f artifacts/authentication-frontend-enable-mtls.yml -n project-1
```
- Check that access to frontend app via route will failed
```bash
curl -v $FRONTEND_URL
```
- Check that access to frontend app via istio gateway
```bash
curl $GATEWAY_URL
```

### Secure Frontend with JWT Authentication

```mermaid
graph LR
    client-->istio-gateway
    istio-gateway--JWT Authentication-->frontend-v2
    istio-gateway--JWT Authentication-->frontend-v1
    subgraph demo-istio-system
    istio-gateway
    end

    frontend-v1--mTLS-->backend-v1
    frontend-v2--mTLS-->backend-v1
    subgraph project-1
    frontend-v1
    frontend-v2
    end
    subgraph project-2
    backend-v1
    end
    backend-v1-->https://httpbin.org/status/200
```
- Apply JWT authentication policy
```bash
oc apply -f artifacts/frontend-jwt-authentication.-with-mtls.yml -n project-1
```
 - Test with cURL to Gateway's URL without token
```bash
curl $GATEWAY_URL
```
 - Test with cURL to Gateway's URL with invalid token.
```bash
curl -H "Authorization: Bearer $(cat artifacts/jwt-wrong-realm.txt)" $GATEWAY_URL
```
- Check Kiali Graph. Enable Show Badge Security. Graph will display key icon which identify mTLS
  
  ![](images/../artifacts/graph-all-secured.png)

- Use jwt.io to check token
  - Test with cURL to Gateway's URL with valid token.
```bash
curl -H "Authorization: Bearer $(cat artifacts/jwt.txt)" $GATEWAY_URL
```
   - Use jwt.io to check token
      - Valid Token
      
        ![valid token](images/valid-jwt.png)

      - Invalid Token
      
        ![invalid token](images/invalid-jwt.png)

### Istio Egress Gateway

- By default Istio allow request go outside Service Mesh. This configuration is in configmap **istio** in Istio Control Plane project.

- Check configmap **istio**
```bash
oc get configmap istio -n demo-istio-system -o jsonpath='{.data.mesh}' | grep "mode: ALLOW_ANY"
```
You will get output similar to this
```yaml
mode: ALLOW_ANY
```
- Change behavior of Istio to **locking-by-default** policy by change from *ALLOW_ANY* to *REGISTRY_ONLY* by run following command
    - by CLI
    ```bash
    oc get configmap istio -n demo-istio-system -o yaml \
    | sed 's/mode: ALLOW_ANY/mode: REGISTRY_ONLY/g' \
    | oc replace -n demo-istio-system -f -
    ```
    - by Admin Console
        - Switch to Admin Console, select project demo-istio-system, Workloads -> Config Maps
        - Select istio
        - Find "mode: ALLOW_ANY" and replace with "mode: REGISTRY_ONLY"

          ![config map istio](images/config-map-istio.png)
        
- Test with cURL
```bash
curl -H "Authorization: Bearer $(cat artifacts/jwt.txt)" $GATEWAY_URL
```

- Check error message from backend
```bash
Host:backend-v1-698588d95-sk2xw, Status:503, Message: Remote host terminated the handshake
```
- Create ServiceEntry to allow egress traffic to httpbin.org and allow only HTTPS and port 443. Check for [egress-serviceentry.yml](artifacts/egress-serviceentry.yml)
```yaml
apiVersion: networking.istio.io/v1alpha3
kind: ServiceEntry
metadata:
  name: http.bin
spec:
  hosts:
  - httpbin.org
  ports:
  - number: 443
    name: https
    protocol: HTTPS
  resolution: DNS
  location: MESH_EXTERNAL
```

- Apply [egress-serviceentry.yml](artifacts/egress-serviceentry.yml) and test cURL
```bash
oc apply -f artifacts/egress-serviceentry.yml -n project-2
curl -H "Authorization: Bearer $(cat artifacts/jwt.txt)" $GATEWAY_URL
# Check that Response code is 200
```
- Test with script
```bash
scripts/run-20-with-jwt.sh
scripts/run-with-jwt.sh
```
- Check Kiali Console Graph for egress **http.bin** 
- Change control plane back to ALLOW_ALL
```bash
 oc get configmap istio -n demo-istio-system -o yaml \
  | sed 's/mode: REGISTRY_ONLY/mode: ALLOW_ANY/g' \
  | oc replace -n demo-istio-system -f -
```

## Cleanup
- Run [cleanup.sh](scripts/cleanup.sh)
```bash
oc delete policy  --all -n project-1
oc delete policy  --all -n project-2
oc delete gateway --all  -n project-1
oc delete serviceentry --all  -n project-2
oc delete destinationrule --all  -n project-1
oc delete destinationrule --all  -n project-2
oc delete virtualservice --all  -n project-2
oc delete virtualservice --all  -n project-1
oc delete smmr --all -n demo-istio-system
oc delete smcp --all -n demo-istio-system
```